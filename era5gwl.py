
import numpy as N
import matplotlib.pyplot as P
from mpl_toolkits.basemap import Basemap, maskoceans
from netCDF4 import Dataset, num2date,date2num
from matplotlib.offsetbox import AnchoredText
from scipy.stats import linregress

def era5gwl(gws):

    para = 'tp'

    # Setting: colormap and color range

    cols = {'tp':['YlGnBu',0,2000,'PuOr',-200,200,'mm']}

    # Reading: ERA5 input data

    file = 'era5_%s_daily_1950-2023_EU.nc'%para

    nc = Dataset(file,'r')

    dat = N.array(nc.variables[para][:])
    lon = N.array(nc.variables['lon'][:]);nx = len(lon)
    lat = N.array(nc.variables['lat'][:]);ny = len(lat)

    lon = lon-360

    tim = nc.variables['time']

    tim = num2date(tim[:], units=tim.units,calendar=tim.calendar)

    jj = []

    for it in tim:

        jj.append(int(it.year))

    jj = N.array(jj)

    nc.close()

    id = N.where((jj>=1981)&(jj<=2023))[0]

    dat = dat[id,:,:]

    # Reading: GWL timeseries

    file = 'gwlneudatum.dat'
    gwl = N.genfromtxt(file,names=True,dtype=None)

    jj = N.array(gwl['ja'])
    mm = N.array(gwl['mo'])
    dd = N.array(gwl['ta'])
    gw = N.array(gwl['gw'],str)

    id = N.where((jj>=1981)&(jj<=2023))[0]

    jj = jj[id]
    mm = mm[id]
    dd = dd[id]
    gw = gw[id]
    
    # Date: sychronized

    # GWL: combine weather-types

    gw[gw=='U'] = 'W'
    gw[gw=='WA'] = 'W'
    gw[gw=='WZ'] = 'W'
    gw[gw=='WS'] = 'W'
    gw[gw=='WW'] = 'W'

    gw[gw=='NWZ'] = 'NW'
    gw[gw=='NWA'] = 'NW'

    gw[gw=='SWZ'] = 'SW'
    gw[gw=='SWA'] = 'SW'

    gw[gw=='NZ'] = 'N'
    gw[gw=='NA'] = 'N'

    gw[gw=='SZ'] = 'S'
    gw[gw=='SA'] = 'S'

    gw[gw=='SEZ'] = 'SE'
    gw[gw=='SEA'] = 'SE'

    gw[gw=='NEZ'] = 'NE'
    gw[gw=='NEA'] = 'NE'

    gw[gw=='HNA'] = 'HN'
    gw[gw=='HNZ'] = 'HN'

    gw[gw=='HNFA'] = 'HNF'
    gw[gw=='HNFZ'] = 'HNF'

    gw[gw=='HFA'] = 'HF'
    gw[gw=='HFZ'] = 'HF'

    gw[gw=='TM'] = 'TRM'

    mo = N.arange(1,13,1)
    nm = len(mo)

    go = N.array(list(set(gw)))
    ng = len(go)

    jo = N.arange(1981,2024,1)
    nj = len(jo)

    nd = len(dd)

    # Calculate averaged daily precipitation per weather-type

    avg = N.zeros((ny,nx,nm,ng),float)

    for g in range(ng):
        for m in range(nm):

            id = N.where((gw==go[g])&(mm==mo[m]))[0]

            avg[:,:,m,g] = N.nanmean(dat[id,:,:],0)

    # Calculate daily fields of mean precipitation  

    gwl = N.zeros((nd,ny,nx),float)

    for d in range(nd):

        ig = N.where(go==gw[d])[0]

        if(len(ig)!=0):

            im = mm[d]-1

            gwl[d,:,:] = avg[:,:,im,ig[0]]

    sum0 = N.zeros((nj,ny,nx),float)
    avg0 = N.zeros((nj,ny,nx),float)

    gx = N.zeros(nj,float)
    
    # Calculate annual sum for prescribed filter condidtions

    for j in range(nj):

        if(gws=='all'): id = N.where((jj==jo[j]))[0]
        else: id = N.where((jj==jo[j])&(gw==gws))[0]

        if(para=='tp'): sum0[j,:,:] = N.nansum(dat[id,:,:],0)#-N.nansum(gwl[id,:,:],0)
        if(para=='t2m'): sum0[j,:,:] = N.nanmean(dat[id,:,:],0)-N.nanmean(gwl[id,:,:],0)
        avg0[j,:,:] = N.nanmean(dat[id,:,:],0)-N.nanmean(gwl[id,:,:],0)

        id = N.where((jj==jo[j])&(gw==gws))[0]

        gx[j] = len(id)

    # Calculate linear trends of weather-types

    stats = linregress(N.linspace(-0,1,nj),gx)

    gs = stats[0]
    gi = stats[1]
    gp = stats[3]

    # Calculate linear trends of total precipitation

    avg1 = N.zeros((ny,nx),float)
    trd1 = N.zeros((ny,nx),float)
    val1 = N.zeros((ny,nx),float)

    for y in range(ny):

        print (lat[y])

        for x in range(nx):

            stats = linregress(N.linspace(0,1,nj),sum0[:,y,x])

            avg1[y,x] = stats[1]
            trd1[y,x] = stats[0]
            val1[y,x] = stats[3]

    # Plotting results

    fig = P.figure(figsize=(13,5))

    if(para=='tp'): fig.suptitle('ERA5 | Annual Precipitation | 1981-2023 | Long-Term Trends\n%s: a=%i | b=%i | p=%.2f'%(gws,gi,gs,gp),fontsize=16,weight='bold')
    if(para=='t2m'): fig.suptitle('ERA5 | Annual Mean Temperature | 1981-2023 | Long-term Trends',fontsize=16,weight='bold')

    ax = P.subplot(121)

    m = Basemap(projection='cyl',llcrnrlat=30,urcrnrlat=70,llcrnrlon=-20,urcrnrlon=40,resolution='l')
    m.drawcoastlines(color='k',linewidth=0.4,zorder=5)
    m.drawcountries(color='k',linewidth=0.4,zorder=5)

    lons,lats = N.meshgrid(lon,lat)

    xx,yy = m(*N.meshgrid(lon,lat))

    im = m.pcolormesh(xx,yy,avg1,cmap=P.get_cmap(cols[para][0]),vmin=cols[para][1],vmax=cols[para][2],zorder=4)

    at = AnchoredText('Intercept | %s'%cols[para][6],prop=dict(size=12,weight='bold'),frameon=True,loc='upper left')
    at.patch.set_boxstyle("round,pad=0.,rounding_size=0.1")
    ax.add_artist(at)

    cb = fig.colorbar(im,location='left',shrink=0.4,pad=-0.2,anchor=(0.3,0.15),extend='both')
    #cb.set_ticks([0,100,500,1000,1500,2000,2500])
    cb.ax.tick_params(labelsize=10,labelcolor='r',color='r')
    cb.outline.set_color('r')
    #cb.set_label('mm',y=1.1,rotation=0,fontsize=10,weight='bold')

    ax = P.subplot(122)

    m = Basemap(projection='cyl',llcrnrlat=30,urcrnrlat=70,llcrnrlon=-20,urcrnrlon=40,resolution='l')
    m.drawcoastlines(color='k',linewidth=0.4,zorder=5)
    m.drawcountries(color='k',linewidth=0.4,zorder=5)

    lons,lats = N.meshgrid(lon,lat)

    xx,yy = m(*N.meshgrid(lon,lat))

    im = m.pcolor(xx,yy,trd1,cmap=P.get_cmap(cols[para][3]),vmin=cols[para][4],vmax=cols[para][5],zorder=4)

    for y in range(ny):
        for x in range(nx):

            if(val1[y,x]<0.05):

               P.scatter(xx[y,x],yy[y,x],s=3,ec='k',c='None',lw=0.2,zorder=5)

    cb = fig.colorbar(im,location='left',shrink=0.4,pad=-0.2,anchor=(0.3,0.15),extend='both')
    cb.ax.tick_params(labelsize=10,labelcolor='r',color='r')
    cb.outline.set_color('r')

    at = AnchoredText('Slope | %s'%cols[para][6],prop=dict(size=12,weight='bold'),frameon=True,loc='upper left')
    at.patch.set_boxstyle("round,pad=0.,rounding_size=0.1")
    ax.add_artist(at)

    P.tight_layout()
    P.savefig('era5gwl.png',format='png',dpi=240,transparent=True)
    return

# Running program for selected weather-types

#era5gwl('all')
era5gwl('W')

