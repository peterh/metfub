# MetFUB


## Übung

### Auswertungen von Niederschlagstrends über Europa unter Berücksichtigung veränderter Großwetterlagen

### Hintergrund

Bei der Untersuchung von Jahreszeitlichen Niederschlagsentwicklungen spielen großräumige Wettermuster als Ursache in der Regel eine eher untergeordnete Rolle. Sind sie aber doch aber dafür verantwortlich ob es örtlich Regen gibt oder trocken bleibt. Unter Berücksichtigung von Merkmalen wiederkehrender Großwetterbedingungen kann die Analyse verfeinert werden, um Ursachen für Veränderungen aufzudecken.

### Aufgabe

Berechnen Sie Jahressummen im Niederschlag über Europa und Analysieren Sie das Trendmuster von 1981-2023. Wo ist der Trend zurückgegangen, und wo hat er zugenommen?

Nutzen Sie nun die Zeitreihe der Wetterlagenklassifikation, um die Niederschlagstrends für einzelne dominante Wetterlagen darzustellen. Beginnen sie mit WZ (west-zyklonal). Bilden Sie erneut Jahressummen für Tage mit dem Merkmal WZ. Interpretieren Sie das Trendmuster? Worauf ist die Änderung maßgeblich zurückzuführen?

Wiederholen Sie die Trendanalyse erneut und schließen Sie nun Tage mit dem Merkmal WZ aus. Welche Folgen hätte es für Europa, wenn es kein WZ Großwetter gäbe?

### Daten

* tägliche ERA5 Reanalysefelder der Tagesniederschlagssumme
* tägliche Zeitreihe klassifizierter Großwetterlagen über Europa nach Hess/Brezowsky

### Hinweise

* ERA5 Reanalysefelder sind mit CDO für Europa zuzuschneiden
* synchronisieren Sie die Datumsangaben in den Datensätzen

### Ergebnisse

#### Trendmuster:

|Gesamt|nur W|
|---|---|
|![](era5gwl_all.png)|![](era5gwl_W.png)|






